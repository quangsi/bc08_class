import { layThongTinTuForm } from "../v1/controller-v1.js";
import { BASE_URL, fetchFoodList, showMessage, showThongTinForm } from "./controller-v2.js";

// render ds món ăn MỚI NHẤT từ server
fetchFoodList();

let deleteFood = (id) => {
  console.log(id);
  axios
    .delete(`${BASE_URL}/${id}`)
    .then((res) => {
      // gọi lại api lấy danh sách mới nhất từ server và re-render
      fetchFoodList();
      showMessage("Xoá thành công");
    })
    .catch((err) => {
      console.log(err);
      showMessage("Đã có lỗi xảy ra", false);
    });
};
window.deleteFood = deleteFood;

let addFood = () => {
  let data = layThongTinTuForm();
  axios
    .post(BASE_URL, data)
    .then((res) => {
      console.log(res);
      $("#exampleModal").modal("hide");
      showMessage("Thêm thành công");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.addFood = addFood;

window.editFood = (id) => {
  // mở modal để fill data từ server
  $("#exampleModal").modal("show");
  // chặn user edit mã món
  document.getElementById("foodID").readOnly = true;

  // getById
  let url = `${BASE_URL}/${id}`;
  axios
    .get(url)
    .then((res) => {
      console.log(res);
      showThongTinForm(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.updateFood = () => {
  let data = layThongTinTuForm();
  axios
    .put(`${BASE_URL}/${data.ma}`, data)
    .then((res) => {
      console.log(res);
      $("#exampleModal").modal("hide");
      showMessage("update thành công");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
